package com.pabdi.service.impl;

import com.pabdi.entity.Role;
import com.pabdi.entity.User;
import com.pabdi.repository.impl.UserRepository;
import com.pabdi.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.*;

//@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    private User expUser;

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Before
    public void init(){
        expUser = new User();
        expUser.setUserLogin("admin");
        expUser.setUserRole(Role.ADMIN);
        userService = new UserServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getById() {
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(expUser));
        userService.getById(1L);
        Mockito.verify(userRepository, Mockito.atLeastOnce()).findById(1L);
        Mockito.verifyNoMoreInteractions(userRepository);
    }
}