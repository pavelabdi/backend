package com.pabdi.repository.impl;

import java.util.List;
import java.util.Optional;

import com.pabdi.repository.AbstractRepository;
import com.pabdi.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository extends AbstractRepository<Long, User> {

    public UserRepository(){

    }

    public Optional<User> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<User> findUserByLogin(String login) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM User u WHERE u.userLogin =:login", User.class)
                .setParameter("login", login)
                .getSingleResult());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<User>> findAllUsers() {
        return Optional.of(getAll());
    }

    public User save(User user) {
        return persist(user);
    }

    public User change(User user) {return update(user);}
}