package com.pabdi.repository.impl;

import com.pabdi.entity.Attachment;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AttachmentRepository extends AbstractRepository<Long, Attachment> {

    public AttachmentRepository(){

    }

    public Optional<Attachment> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<Attachment> findAttachmentByName(String name) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Attachment i WHERE i.attachmentName =:name", Attachment.class)
                .setParameter("name", name)
                .getSingleResult());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Attachment>> findAllAttachments() {
        return Optional.of(getAll());
    }

    public Attachment save(Attachment attachment) {
        return persist(attachment);
    }
}
