package com.pabdi.repository.impl;

import com.pabdi.entity.Order;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class OrderRepository extends AbstractRepository<Long, Order> {

    public OrderRepository(){

    }

    public Optional<Order> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Order>> findAllOrders() {
        return Optional.ofNullable(getAll());
    }

    public Order save(Order order) {
        return persist(order);
    }
}
