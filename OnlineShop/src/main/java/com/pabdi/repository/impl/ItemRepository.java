package com.pabdi.repository.impl;

import java.util.List;
import java.util.Optional;

import com.pabdi.entity.Item;
import com.pabdi.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ItemRepository extends AbstractRepository<Long, Item> {

    public ItemRepository(){

    }

    public Optional<Item> findById(Long id) {
        return Optional.ofNullable(getByKey(id));
    }

    public Optional<Item> findItemByName(String name) {
        return Optional.ofNullable(getEntityManager()
                .createQuery("FROM Item i WHERE i.itemName =:name", Item.class)
                .setParameter("name", name)
                .getSingleResult());
    }

    @SuppressWarnings("unchecked")
    public Optional<List<Item>> findAllItems() {
        return Optional.ofNullable(getAll());
    }

    public Item save(Item item) {
        return persist(item);
    }
}