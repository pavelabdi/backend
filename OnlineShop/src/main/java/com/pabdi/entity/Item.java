package com.pabdi.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long itemId;

    @Column(name = "name")
    private String itemName;

    @Column(name = "price")
    private BigDecimal itemPrice;

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private Category itemCategory;

    public Item(){

    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String name) {
        this.itemName = name;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal price) {
        this.itemPrice = price;
    }

    public Category getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(Category category) {
        this.itemCategory = category;
    }
}
