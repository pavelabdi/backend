package com.pabdi.controller;

import com.pabdi.dto.OrderDto;
import com.pabdi.entity.Item;
import com.pabdi.entity.Order;
import com.pabdi.mapper.OrderMapper;
import com.pabdi.service.OrderService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private List<String> selectedItems = new ArrayList<>();

    private List<Item> orderItems = new ArrayList<>();

    private final OrderService orderService;

    private final OrderMapper orderMapper;

    public OrderController(OrderMapper orderMapper, @Qualifier("orderService") OrderService orderService){
        this.orderMapper = orderMapper;
        this.orderService = orderService;
    }

    @GetMapping
    public ResponseEntity<List<OrderDto>> getAll() {
        List<Order> orderEntities = orderService.getAll();
        return ResponseEntity.ok(orderEntities.stream()
                .map(orderMapper::toDto)
                .collect(Collectors.toList()));
    }

    @PostMapping
    public ResponseEntity<Order> save(@RequestBody OrderDto order){
        return ResponseEntity.ok(orderService.add(orderMapper.toEntity(order)));
    }
}
