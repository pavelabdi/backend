package com.pabdi.controller;

import com.pabdi.dto.ItemDto;
import com.pabdi.entity.Item;
import com.pabdi.mapper.ItemMapper;
import com.pabdi.service.ItemService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/welcome")
public class ItemController {

    private final ItemService itemService;

    private final ItemMapper itemMapper;

    public ItemController(ItemMapper itemMapper, @Qualifier("itemService") ItemService itemService){
        this.itemMapper = itemMapper;
        this.itemService = itemService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ItemDto> getById(@PathVariable Long id){
        Item itemEntity = itemService.getById(id);
        return ResponseEntity.ok(itemMapper.toDto(itemEntity));
    }

    @GetMapping
    public ResponseEntity<List<ItemDto>> getAll(){
        List<Item> itemEntities = itemService.getAll();
        return ResponseEntity.ok(itemEntities.stream()
                .map(itemMapper::toDto)
                .collect(Collectors.toList()));
    }

    @PostMapping
    public ResponseEntity<Item> save(@RequestBody ItemDto item){
        return ResponseEntity.ok(itemService.add(itemMapper.toEntity(item)));
    }
}
