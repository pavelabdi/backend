package com.pabdi.mapper;

import com.pabdi.dto.UserDto;
import com.pabdi.entity.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final BCryptPasswordEncoder passwordEncoder;

    public UserMapper(BCryptPasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    public User toEntity(UserDto dto){
        User newUser = new User();
        newUser.setUserLogin(dto.getName());
        newUser.setUserPassword(passwordEncoder.encode(dto.getPassword()));
        newUser.setUserRole(dto.getRole());
        return newUser;
    }

    public UserDto toDto(User entity){
        UserDto newUserDto = new UserDto();
        newUserDto.setId(entity.getUserId());
        newUserDto.setName(entity.getUserLogin());
        newUserDto.setPassword(entity.getUserPassword());
        newUserDto.setRole(entity.getUserRole());
        return newUserDto;
    }
}
