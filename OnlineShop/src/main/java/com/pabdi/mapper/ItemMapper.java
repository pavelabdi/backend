package com.pabdi.mapper;

import com.pabdi.dto.ItemDto;
import com.pabdi.entity.Item;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper {

    public ItemMapper() {

    }

    public Item toEntity(ItemDto dto) {
        Item newItem = new Item();
        newItem.setItemName(dto.getName());
        newItem.setItemPrice(dto.getPrice());
        return newItem;
    }

    public ItemDto toDto(Item entity) {
        ItemDto newItemDto = new ItemDto();
        newItemDto.setId(entity.getItemId());
        newItemDto.setName(entity.getItemName());
        newItemDto.setPrice(entity.getItemPrice());
        return newItemDto;
    }
}


