package com.pabdi.mapper;

import com.pabdi.dto.OrderDto;
import com.pabdi.entity.Item;
import com.pabdi.entity.Order;
import com.pabdi.service.ItemService;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class OrderMapper {

    private final ItemService itemService;

    private final UserService userService;

    public OrderMapper(@Qualifier("itemService") ItemService itemService, @Qualifier("userService") UserService userService) {
        this.itemService = itemService;
        this.userService = userService;
    }

    public Order toEntity(OrderDto dto) {
        Order newOrder = new Order();
        newOrder.setItems(dto.getItems()
                .stream()
                .map(itemService::getByName)
                .collect(Collectors.toList()));
        newOrder.setOrderUser(userService.getById(1L));
        return newOrder;
    }

    public OrderDto toDto(Order entity) {
        OrderDto newUserDto = new OrderDto();
        newUserDto.setId(entity.getOrderId());
        newUserDto.setItems(entity.getItems()
                .stream()
                .map(Item::getItemName)
                .collect(Collectors.toList()));
        return newUserDto;
    }
}
