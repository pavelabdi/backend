package com.pabdi.service;

import com.pabdi.entity.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(Long id);

    User getByLogin(String name);

    User add(User entity);

    User update(User entity, Long id);
}
