package com.pabdi.service;

import com.pabdi.entity.Item;

import java.util.List;

public interface ItemService {

    List<Item> getAll();

    Item getById(Long id);

    Item getByName(String name);

    Item add(Item entity);
}
