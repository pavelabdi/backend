package com.pabdi.service;

import com.pabdi.entity.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAll();

    Order getById(Long id);

    Order add(Order entity);
}
