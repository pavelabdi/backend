package com.pabdi.service.impl;


import com.pabdi.entity.Attachment;
import com.pabdi.repository.impl.AttachmentRepository;
import com.pabdi.service.AttachmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository){
        this.attachmentRepository = attachmentRepository;
    }

    @Override
    public List<Attachment> getAll() {
        return attachmentRepository.findAllAttachments()
                .orElseThrow(() -> new NoResultException("No attachments found"));
    }

    @Override
    public Attachment getById(Long id) {
        return attachmentRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No attachments found with id " + id));
    }

    @Override
    public Attachment getByName(String name) {
        return attachmentRepository.findAttachmentByName(name)
                .orElseThrow(() -> new NoResultException("No attachment found with name " + name));
    }

    @Override
    public Attachment add(Attachment attachment){
        return attachmentRepository.save(attachment);
    }
}
