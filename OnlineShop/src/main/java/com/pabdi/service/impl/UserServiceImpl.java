package com.pabdi.service.impl;

import com.pabdi.entity.User;
import com.pabdi.repository.impl.UserRepository;
import com.pabdi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

//    public UserServiceImpl(UserRepository userRepository){
//        this.userRepository = userRepository;
//    }

    @Override
    public List<User> getAll(){
        return userRepository.findAllUsers()
                .orElseThrow(() -> new NoResultException("No users found"));
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No user found with username " + id));
    }

    @Override
    public User getByLogin(String login) {
        return userRepository.findUserByLogin(login)
                .orElseThrow(() -> new NoResultException("No user found with username " + login));
    }

    @Override
    public User add(User user){
        return userRepository.save(user);
    }

    @Override
    public User update(User newUser, Long id){
        return userRepository.findById(id)
                .map(user -> {
                    user.setUserLogin(newUser.getUserLogin());
                    user.setUserRole(newUser.getUserRole());
                    user.setUserPassword(newUser.getUserPassword());
                    return userRepository.change(user);
                })
                .orElseGet(() -> userRepository.save(newUser));
    }
}
