package com.pabdi.service.impl;

import com.pabdi.entity.Item;
import com.pabdi.repository.impl.ItemRepository;
import com.pabdi.service.ItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("itemService")
@Transactional
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    public ItemServiceImpl(ItemRepository itemRepository){
        this.itemRepository = itemRepository;
    }

    @Override
    public List<Item> getAll() {
        return itemRepository.findAllItems()
                .orElseThrow(() -> new NoResultException("No items found"));
    }

    @Override
    public Item getById(Long id) {
        return itemRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No item found with name " + id));
    }

    @Override
    public Item getByName(String name) {
        return itemRepository.findItemByName(name)
                .orElseThrow(() -> new NoResultException("No item found with username " + name));
    }

    @Override
    public Item add(Item item){
        return itemRepository.save(item);
    }
}
