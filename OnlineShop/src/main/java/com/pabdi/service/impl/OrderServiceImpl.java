package com.pabdi.service.impl;

import com.pabdi.entity.Order;
import com.pabdi.repository.impl.OrderRepository;
import com.pabdi.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> getAll() {
        return orderRepository.findAllOrders()
                .orElseThrow(() -> new NoResultException("No orders found"));
    }

    @Override
    public Order getById(Long id) {
        return orderRepository.findById(id)
                .orElseThrow(() -> new NoResultException("No order found with username " + id));
    }

    @Override
    public Order add(Order order){
        return orderRepository.save(order);
    }
}
