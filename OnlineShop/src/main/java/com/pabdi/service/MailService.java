package com.pabdi.service;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Service
public class MailService {

    private static final String ENCODING = "utf-8";

    private final JavaMailSender mailSender;

    private final TemplateEngine htmlTemplateEngine;

    public MailService(JavaMailSender mailSender, TemplateEngine htmlTemplateEngine){
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
    }

    public void sendAcceptableMail() {
        Context context = new Context(Locale.US);
        context.setVariable("name", "Pasha");
        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, ENCODING);
        try {
            helper.setTo("jstep@yopmail.com");
            String htmlContext = this.htmlTemplateEngine.process("html/ticketDeclined", context);
            helper.setText(htmlContext, true);
            helper.setSubject("Test");
            this.mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
