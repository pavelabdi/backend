package com.pabdi.service;

import com.pabdi.entity.Attachment;

import java.util.List;

public interface AttachmentService {

    List<Attachment> getAll();

    Attachment getById(Long id);

    Attachment getByName(String name);

    Attachment add(Attachment entity);
}
