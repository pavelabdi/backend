package com.pabdi.dto;

import java.util.ArrayList;
import java.util.List;

public class OrderDto {

    private Long id;

    private List<String> items = new ArrayList<>();

    public OrderDto(){

    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
